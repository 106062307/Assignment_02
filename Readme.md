# Software Studio 2019 Spring Midterm Project

## Topic
* Project Name : Raiden

## Website Detail Description

## Basic **Components**
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete Game Process|15%|Y|
|Basic Rules|20%|Y|
|Jucify Mechanisms|15%|Y|
|Animations|10%|Y|
|Particle System|10%|Y|
|Appearance|5%|Y|
|UI / Sound Effect / Leader Board|15%|Y|

# 作品網址：[作品連結](https://106062307.gitlab.io/Assignment_02/)

# Components Description :

## Complete Game Process

### Start State

- 開始畫面
- 開始畫面背景會移動
- 開始按鈕：按下後可以跳到 Main Game，開始遊戲
- Leader Board：按下後等一下可以看到前 10 名的資料，透過 alart 的形式

### Main Game

#### 操作方式

- Arrow Key：方向鍵可以移動船艦，上下左右
- Space Bar：可以發射子彈
- Z：大絕充能滿了以後可以按下使用大絕招

#### Feature

##### 船艦相關

1. Ship 後面會有噴射氣體的動畫
2. Ship 死掉後會有爆炸的 Animation
3. Ship 重生後會有一段無敵時間，此時間敵人不會發射子彈
4. Ship 可以發射子彈
5. Ship 與 Enemy 相撞後會死掉
6. 大絕招：絕招是讓空間中所有敵人死去，要發動絕招的話要先殺死 20 個敵人以上，螢幕上的大絕沖能就會變成 `Ultimate Ready!!! (Press Z to Fire It!)`

##### 敵人相關

1. 每個敵人接觸到子彈後就會死亡，該發子彈也沒有穿透功能，射到敵人後就會消失
2. 每個敵人會每隔一段時間發射子彈，難度會根據 level 變化
3. 敵人死掉時會有爆炸 animation
4. 敵人死掉時會有 particle effect

##### 關卡難度

- CD 的單會是 ms，因為內部實作是用 setInterval，並沒有使用 phaser 的 timer 做統一 clk 控制。

- enemyRespawnAmount 代表的意義，是每一次做生成敵人這個動作時一次產生幾個敵人出來

- 每一隻 Enemy 都會有自己的 base Attack Speed，關卡難度會統一提升每隻 Enemy 的子彈速度。同時每一隻 Enemy 也會有 Attack CD, 一樣透過 ratio 的形式統一調整。

###### level 1
```
gameSpeed: 1,
enemyRespawnCD: 1000,
enemyRespawnAmount: 1,
enemyAttackSpeedRatio: 1,
enemyAttackCDRatio: 1
```

###### level 2
```
gameSpeed: 2,
enemyRespawnCD: 800,
enemyRespawnAmount: 2,
enemyAttackSpeedRatio: 1,
enemyAttackCDRatio: 1
```

###### level 3
```
gameSpeed: 2,
enemyRespawnCD: 600,
enemyRespawnAmount: 4,
enemyAttackSpeedRatio: 1.5,
enemyAttackCDRatio: 3 / 4
```

###### level 4
```
gameSpeed: 3,
enemyRespawnCD: 600,
enemyRespawnAmount: 4,
enemyAttackSpeedRatio: 2,
enemyAttackCDRatio: 1 / 2
```

##### UI

有 4 個鍵，分別為
- Mute
- Pause
- Volume Up
- Volume Down

##### Music

- 進到 Main Game 會播雷電的歌
- Ship 子彈發射會有聲音

### End Game

- 結束畫面
- 重新開始按鈕：按下後可以跳到 Main Game，重新開始遊戲
- Leader Board：按下後等一下可以看到前 10 名的資料，透過 alart 的形式
- Home Page：按下後可以回到 Start Game state

# Other Functions Description(1~40%) :