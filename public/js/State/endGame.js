// End Game State

class EndGame extends Phaser.State {
  constructor(game) {
    super(game);
  }

  preload() {
    // logo
    this.game.load.image('logo', './assets/game-start-logo.jpg');

    // ui
    this.game.load.image('startIcon', './assets/ui/start-icon.png');
    this.game.load.image('highScoreIcon', './assets/ui/high-score.png');
    this.game.load.image('homeIcon', './assets/ui/house.png');
  }

  create() {
    // logo
    this.game.add.sprite(184, 200, 'logo');

    // ui
    this.game.add.button(this.game.world.centerX - 122, 510, 'startIcon', this.startGame, this);
    this.game.add.button(this.game.world.centerX + 10, 510, 'highScoreIcon', this.leaderBoard, this);
    this.game.add.button(10, 10, 'homeIcon', this.backHome, this);

    // Add Text
    this.game.add.text(this.game.world.centerX - 110, 100, 'GameOver', {
      fill: '#FFFFFF',
      fontSize: 40
    })
  }

  // Callback
  startGame() {
    this.game.stateChange.dispatch('MainGame');
  }

  backHome() {
    this.game.stateChange.dispatch('StartGame');
  }

  async leaderBoard() {
    let scores = await this.game.db.collection('score').orderBy('score', 'desc').limit(10).get();
    let datas = scores.docs;

    // Process Leader Board
    let result = ''
    for (let i = 0; i < datas.length; i++) {
      result += `Rank${i+1} ${datas[i].data().name}: ${datas[i].data().score}${i < datas.length - 1 ? '\n' : ''}`;
    }
    window.alert(result);
  }
}