// Main Game State

class MainGame extends Phaser.State {
  constructor(game) {
    // Init Phaser.State Based Class
    super(game);
  }

  // Init
  init() {
    logger('Init MainGame.');
    
    // Init All Variable
    // Game Score
    this.score = 0;
    this.level = 1;
    this.ultimateValue = 0;

    // Ship
    this.ship = null;

    // Background
    this.background = null;

    // Group
    this.shipGroup = null;
    this.bulletGroup = null;
    this.enemyGroup = null;

    // CreateEnemy
    this.cEnemyInterval = null;
  }

  // Loading Assets
  preload() {
    logger('Load Asset For MainGame.');

    // Background Assets
    this.game.load.image('mainGameBackground', './assets/main-game-background.jpg');

    // Add Space ship Assets
    // Green Ship
    this.game.load.spritesheet('greenShip', './assets/ship/greenShip/green-ship.png', 64, 64);
    this.game.load.spritesheet('greenShipExhaust', './assets/ship/greenShip/green-ship-exhaust.png', 32, 32);
    this.game.load.spritesheet('greenShot', './assets/shot/green-shot.png', 32, 32);

    // Add Enemy
    this.game.load.spritesheet('blackGreenSpider', './assets/enemy/black-green-spider.png', 64, 64);
    this.game.load.spritesheet('enemyExplosion', './assets/enemy/explosion.png', 64, 64);
    this.game.load.image('enemyShot', './assets/enemy/enemy-bullet.png');
    this.game.load.image('orangeParticle', './assets/enemy/explode-particle.png');

    // UI
    this.game.load.image('pause', './assets/ui/pause.png');
    this.game.load.image('mute', './assets/ui/mute.png');
    this.game.load.image('plus', './assets/ui/plus.png');
    this.game.load.image('minus', './assets/ui/minus.png');

    // Audio
    this.game.load.audio('backgroundMusic', './assets/backgroundMusic.mp3');
    this.game.load.audio('fireBullet', './assets/ship/laser1.wav');
  }

  // Initiate After Loading Process
  create() {
    logger('In Create Hook.');

    // Clear Level Setting
    Object.assign(Global.level, Global.defaultLevelInfo);

    // Set World Bound
    this.game.world.setBounds(0, 0, Global.worldSize.width, Global.worldSize.height);

    // Set Up UI
    this.game.add.button(Global.worldSize.width + 10, 10, 'pause', this.gamePause, this);
    this.game.add.button(Global.worldSize.width + 20 + 64, 10, 'mute', this.muteMusic, this);
    this.game.add.button(Global.worldSize.width + 10, 20 + 64, 'plus', this.volumeUp, this);
    this.game.add.button(Global.worldSize.width + 20 + 64, 20 + 64, 'minus', this.volumeDown, this);

    // Add Audio
    this.music = this.game.add.audio('backgroundMusic');
    this.music.loop = true;
    this.music.play();

    // Set Up Physics Mode
    this.game.physics.startSystem(Phaser.Physics.ARCADE);

    // Add Key Control Callback
    this.game.input.keyboard.createCursorKeys();
    this.game.input.keyboard.addCallbacks(this, this.keyDone, this.keyUp);

    // Setup Background
    this.background = this.game.add.tileSprite(0, 0, Global.worldSize.width, Global.worldSize.height, 'mainGameBackground');

    // Create Bullet Group and Ship Group
    this.shipGroup = this.game.add.group(this.game.world, 'shipGroup');
    this.bulletGroup = this.game.add.group(this.game.world, 'bulletGroup');

    // Init A Ship
    this.ship = new GreenShip(this.game, this.game.world.centerX - 30, this.game.world.centerY + 100, 'greenShip', 'greenShipExhaust', 'greenShot', this.shipGroup, this.bulletGroup);

    this.ship.beforeDestroy.add(this.checkIfChangeState, this);

    // Init A Enemy
    // Create Enemy Group
    this.enemyGroup = this.game.add.group(this.game.world, 'enemyGroup');
    this.enemyBulletGroup = this.game.add.group(this.game.world, 'enemyBulletGroup');

    // Add Text To Main Game
    this.scoreText = this.game.add.text(10, 10, `Current Score: ${this.score}   Remain Life: ${this.ship.life} Level: ${this.level}`, {
      fontSize: 20,
      fill: '#000000'
    });

    this.ultimateText = this.game.add.text(10, 40, '', {
      fontSize: 20,
      fill: '#000000'
    });

    // Start To Create Enemy
    this.createEnemy();
  }

  // Game Update Hook.
  update() {
    // Update Background
    this.background.tilePosition.y += Global.level.gameSpeed;

    // Game Info
    this.scoreText.text = `Current Score: ${this.score}   Remain Life: ${this.ship.life} Level: ${this.level}`;
    this.ultimateText.text = this.ultimateValue >= 20 ? `Ultimate Ready!!! (Press Z to Fire It!)` : `Ultimate Power: ${this.ultimateValue}`;

    // Check For Overlap
    this.game.physics.arcade.overlap(this.bulletGroup, this.enemyGroup, this.killEnemy, null, this);
    this.game.physics.arcade.overlap(this.enemyBulletGroup, this.shipGroup, this.killShip, null, this);
    if (!this.ship.isDying) this.game.physics.arcade.overlap(this.enemyGroup, this.shipGroup, this.killShipWhenEnemyOverlap, null, this);

    // Add Fire Weapon Check At Here
    if (this.game.input.keyboard.isDown(Phaser.KeyCode.SPACEBAR) && this.ship.canOperate) this.ship.fire();
  }

  // Game ShutDown Hook
  shutdown() {
    // Clear All Timeout
    clearInterval(this.cEnemyInterval);

    // Get Name
    let playerName = window.prompt('請輸入名稱', '');

    this.enemyGroup.children.forEach(e => e.clearFire());

    // Clear All Asset
    this.music.stop();

    // Clear Bound Setting
    this.game.world.setBounds(0, 0, Global.worldSize.width + Global.worldSize.uiWidth, Global.worldSize.height);

    // Send Score To Server
    this.game.db.collection('score').add({
      score: this.score,
      name: playerName
    })
  }

  // Game Logic
  /**
   * @name createEnemy Use To Generate Enemy
   */
  createEnemy() {
    this.cEnemyInterval = setInterval(() => {
      // First, Random Position
      let points = [];
      let max = this.game.world.bounds.width - 64;
      let min = 0;
      for (let i = 0; i < Global.level.enemyRespawnAmount; i++) {
        points.push(Math.floor(Math.random() * (max - min)) + min);
      }

      // Create Enemy
      for (let i = 0; i < Global.level.enemyRespawnAmount; i++) {
        new BlackGreenSpider(this.game, points[i], -64, 'blackGreenSpider', 'enemyExplosion', 'enemyShot', 'orangeParticle', this.enemyGroup, this.enemyBulletGroup, this.ship);
      }
    }, Global.level.enemyRespawnCD);
  }

  // Game Miscellaneous Callback Definition 
  volumeUp() {
    if (this.music.volume < 1) this.music.volume += 0.05;
    if (this.ship.volume < 1) this.ship.volume += 0.05;
  }

  volumeDown() {
    if (this.music.volume > 0) this.music.volume -= 0.05;
    if (this.ship.volume > 0) this.ship.volume -= 0.05;
  }
  
  muteMusic() {
    this.music.mute = !this.music.mute;
    this.ship.mute = !this.ship.mute;
  }

  gamePause() {
    this.game.paused = true;
    clearInterval(this.cEnemyInterval);

    this.enemyGroup.children.forEach(e => e.clearFire());
  }

  gameResume() {
    this.game.paused = false;
    this.createEnemy();
    this.enemyGroup.children.forEach(e => e.startFire());
  }

  /**
   * @name checkIfChangeState
   */
  checkIfChangeState() {
    if (this.shipGroup.length === 0) {
      this.game.stateChange.dispatch('EndGame');
    }
  }
  /**
   * @name killEnemy
   * @param { Phaser.Sprite } bullet 
   * @param { Phaser.Sprite } enemy 
   */
  killEnemy(bullet, enemy) {
    bullet.kill();
    enemy.die();

    // Update Score
    this.score += 20;
    this.ultimateValue += 1;
    if (this.ultimateValue == 20) this.ultimateTextTween = this.game.add.tween(this.ultimateText).to({ 'alpha': 0 }).repeat(-1).yoyo(true).start();

    // Update Level
    if (this.score >= 2600) {
      Object.assign(Global.level, {
        gameSpeed: 3,
        enemyRespawnCD: 600,
        enemyRespawnAmount: 4,
        enemyAttackSpeedRatio: 2,
        enemyAttackCDRatio: 1 / 2
      });
      this.level = 4;
    } else if (this.score >= 1200) {
      Object.assign(Global.level, {
        gameSpeed: 2,
        enemyRespawnCD: 600,
        enemyRespawnAmount: 4,
        enemyAttackSpeedRatio: 1.5,
        enemyAttackCDRatio: 3 / 4
      });
      this.level = 3;
    } else if (this.score >= 400) {
      Object.assign(Global.level, {
        gameSpeed: 2,
        enemyRespawnCD: 800,
        enemyRespawnAmount: 2
      });
      this.level = 2;
    }
  }

  killShip(bullet, ship) {
    bullet.kill();
    this.ship.explode();
  }

  killShipWhenEnemyOverlap() {
    this.ship.explode();
  }

  /**
   * @name ultimate ship ultimate --> Kill All Enemy
   */
  ultimate() {
    this.enemyGroup.children.forEach(e => {
      e.die();
      this.score += 20;
    });
    this.enemyBulletGroup.children.forEach(e => e.kill());
    this.ultimateValue = 0;
    this.game.tweens.remove(this.ultimateTextTween);
    this.ultimateText.alpha = 1;
  }

  /**
   * @name keyDone
   * @param { KeyboardEvent } event
   */
  keyDone(event) {
    let { key } = event;

    if (this.ship.canOperate) {
      if (!this.game.paused) {
        if(key === 'ArrowLeft') this.ship.goLeft();
        if(key === 'ArrowRight') this.ship.goRight();
        if(key === 'ArrowDown') this.ship.goDown();
        if(key === 'ArrowUp') this.ship.goUp();
        if(key === 'z' && this.ultimateValue >= 20) this.ultimate();
      } else {
        if (key === ' ') this.gameResume();
      }
    }
  }

  /**
   * @name keyUp
   * @param { KeyboardEvent } event
   */
  keyUp(event) {
    let { key } = event;
    let { keyboard } = this.game.input;
    let { KeyCode } = Phaser;

    if (this.game.paused) return;
    
    if (!keyboard.isDown(KeyCode.RIGHT) && !keyboard.isDown(KeyCode.LEFT)) this.ship.clearVelocityX();
    if (!keyboard.isDown(KeyCode.UP) && !keyboard.isDown(KeyCode.DOWN)) this.ship.clearVelocityY();
  }
}