// Start Menu

class StartMenu extends Phaser.State {
  constructor(game) {
    // Init Phaser.State Based Class
    super(game);
  }

  preload() {
    // Background Assets
    this.game.load.image('mainGameBackground', './assets/main-game-background.jpg');
    
    // logo
    this.game.load.image('logo', './assets/game-start-logo.jpg');

    // ui
    this.game.load.image('startIcon', './assets/ui/start-icon.png');
    this.game.load.image('highScoreIcon', './assets/ui/high-score.png');
  }

  create() {
    // Clear Level Setting
    Object.assign(Global.level, Global.defaultLevelInfo);
    
    // Setup Background
    this.background = this.game.add.tileSprite(Global.worldSize.uiWidth / 2, 0, Global.worldSize.width, Global.worldSize.height, 'mainGameBackground');

    // logo
    this.game.add.sprite(184, 200, 'logo');

    // ui
    this.game.add.button(this.game.world.centerX - 122, 510, 'startIcon', this.startGame, this);
    this.game.add.button(this.game.world.centerX + 10, 510, 'highScoreIcon', this.leaderBoard, this);

    // Add Text
    this.game.add.text(this.game.world.centerX - 110, 100, 'Raiden 雷電', {
      fill: '#FFFFFF',
      fontSize: 40
    });
  }

  update() {
    // Update Background
    this.background.tilePosition.y += Global.level.gameSpeed;
  }

  // Callback
  startGame() {
    this.game.stateChange.dispatch('MainGame');
  }

  async leaderBoard() {
    let scores = await this.game.db.collection('score').orderBy('score', 'desc').limit(10).get();
    let datas = scores.docs;

    // Process Leader Board
    let result = ''
    for (let i = 0; i < datas.length; i++) {
      result += `Rank${i+1} ${datas[i].data().name}: ${datas[i].data().score}${i < datas.length - 1 ? '\n' : ''}`;
    }
    window.alert(result);
  }
}