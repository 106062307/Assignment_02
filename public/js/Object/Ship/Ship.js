// Ship Module

// Game Helper
class Ship extends GameObject {
  // Constant Declaration
  MoveVelocity = 400;
  ShipHealth = 3;

  // Exhaust Related
  ExhaustOffsetX = 15;
  ExhaustOffsetY = 55;
  ExhaustFrameRate = 8;

  // Explode Relate
  ExplodeFrameRate = 16;

  // Shot Relate
  ShotVelocity = 300;
  ShotOffsetX = 0;
  ShotOffsetY = 30;
  ShotCD = 100; // In ms, use for setTimeout

  /**
   * @name constructor 
   * @param { Phaser.Game } game Phaser Game Object. Require.
   * @param { Number } initX Ship Initial X Position. Require.
   * @param { Number } initY Ship Initial Y Position. Require.
   * @param { String } shipAsset Ship Asset Name. Require.
   * @param { String } exhaustAsset Ship's Exhaust Asset Name.
   * @param { Phaser.Group } shipGroup Ship's Group
   * @param { Phaser.Group } bulletGroup Ship's Fire Group
   */
  constructor(game, initX, initY, shipAsset, exhaustAsset, shotAsset, shipGroup, bulletGroup) {
    // Init Sprite Based Class
    super(game);

    // Ship Setting
    this.initX = initX;
    this.initY = initY;
    this.isDying = false;
    this.canOperate = true;

    // Shot Setting
    this.shotAsset = shotAsset;
    this.shotGroup = bulletGroup;

    // Control Flag
    this.canFire = true;

    // Event
    this.beforeDestroy = new Phaser.Signal();

    // Init Ship
    this.ship = this.addSprite({
      x: this.initX,
      y: this.initY,
      key: shipAsset,
      frame: 0,
      group: shipGroup
    });

    // Set Ship Initial Health
    this.life = this.ShipHealth;

    // Add Ship's Death Invincible
    this.shipTween = this.game.add.tween(this.ship);

    this.shipTween.to({
      'alpha': 0
    }, 500, null, false, 0, 3, true);

    this.shipTween.onComplete.add(() => {
      this.isDying = false;
      this.ship.alpha = 1;
    }, this);

    // Init Exhaust
    this.exhaust = this.addSprite({
      x: this.initX + this.ExhaustOffsetX,
      y: this.initY + this.ExhaustOffsetY,
      key: exhaustAsset,
      frame: 0
    })

    // Add Exhaust Tween
    this.exhaustTween = this.game.add.tween(this.exhaust);

    this.exhaustTween.to({
      'alpha': 0
    }, 500, null, false, 0, 3, true);

    this.ship.body.collideWorldBounds = true;
    this.ship.body.onWorldBounds = new Phaser.Signal()
    this.ship.body.onWorldBounds.add((sprite, up, down, left, right) => {
      // Clear Exhaust Velocity
      if (left || right) {
        this.exhaust.body.velocity.x = 0;
        this.exhaust.x = this.x + this.ExhaustOffsetX;
      }
      if (up || down) {
        this.exhaust.body.velocity.y = 0;
        this.exhaust.y = this.y + this.ExhaustOffsetY;
      }
    }, this)

    // Add Exhaust Animation
    this.exhaust.animations.add('exhaustNormal', [0, 1, 2, 3], this.ExhaustFrameRate, true);
    this.exhaust.animations.play('exhaustNormal');

    // Add Explosition
    this.ship.animations.add('explode', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], this.ExplodeFrameRate, false)
        .onComplete.add(this.explodeComplete, this);

    // Add Fire Bullet Audio
    this.fireBulletMusic = this.game.add.audio('fireBullet');
    
    // Add Alias
    this.body = this.ship.body;

    logger(`${shipAsset} Added.`);
  }
  // Public Getter / Setter
  set x(value) {
    this.body.x = value;
  }

  get x() {
    return this.body.x;
  }

  set y(value) {
    this.body.y = value;
  }

  get y() {
    return this.body.y;
  }

  get mute() {
    return this.fireBulletMusic.mute;
  }

  set mute(value) {
    this.fireBulletMusic.mute = value;
  }

  get volume() {
    return this.fireBulletMusic.volume;
  }

  set volume(value) {
    this.fireBulletMusic.volume = value;
  }

  // Public Function
  /**
   * @name goLeft Ship To Left Handler
   */
  goLeft() {
    if (this.life === 0) return;
    this.body.velocity.x = -1 * this.MoveVelocity;
    this.exhaust.body.velocity.x = -1 * this.MoveVelocity;
  }

  /**
   * @name goRight Ship To Right Handler
   */
  goRight() {
    if (this.life === 0) return;
    this.body.velocity.x = this.MoveVelocity;
    this.exhaust.body.velocity.x = this.MoveVelocity;
  }

  /**
   * @name goUp Ship To Up Handler
   */
  goUp() {
    if (this.life === 0) return;
    this.body.velocity.y = -1 * this.MoveVelocity;
    this.exhaust.body.velocity.y = -1 * this.MoveVelocity;
  }

  /**
   * @name goDown Ship Go Down Handler
   */
  goDown() {
    if (this.life === 0) return;
    this.body.velocity.y = this.MoveVelocity;
    this.exhaust.body.velocity.y = this.MoveVelocity;
  }

  /**
   * @name clearVelocityY Invoke When Up/ Down Arrow Key Up
   */
  clearVelocityY() {
    if (this.life === 0) return;
    this.body.velocity.y = 0;
    this.exhaust.body.velocity.y = 0;
  }

  /**
   * @name clearVelocity Invoke When Left / Right Arrow Key Up.
   */
  clearVelocityX() {
    if (this.life === 0) return;
    this.body.velocity.x = 0;
    this.exhaust.body.velocity.x = 0;
  }

  /**
   * @name explode Ship Dead
   */
  explode() {
    if (!this.isDying) {
      this.isDying = true;
      this.canOperate = false;

      // Reset
      this.clearVelocityX();
      this.clearVelocityY();

      // Update ship life
      this.life -= 1;
      if (this.life > 0) {
        this.exhaust.kill();
      } else {
        this.exhaust.destroy();
      }
      this.ship.animations.play('explode');
    }
  }

  /**
   * @name fire Fire Weapon!!!
   */
  fire() {
    // Fire Single Bullet, And Not Record It.
    if (this.canFire) {
      let shot = this.addSprite({
        x: this.x + this.ShotOffsetX,
        y: this.y - this.ShotOffsetY,
        key: this.shotAsset,
        frame: 0,
        group: this.shotGroup
      });

      this.game.physics.enable(shot);

      shot.checkWorldBounds = true;
      shot.outOfBoundsKill = true;

      shot.events.onKilled.add(() => { setTimeout(() => { shot.destroy(); }, 1000) });

      // Add Initial Velocity
      shot.body.velocity.y = -1 * this.ShotVelocity;

      // Play Audio
      this.fireBulletMusic.play();

      // Update Flag And Set Timeout
      this.canFire = false;
      setTimeout(() => { this.canFire = true }, this.ShotCD);
    }
  }

  /**
   * @name reset Used To Reset Ship to Initial status.
   */
  reset() {
    // Reset
    this.ship.x = this.initX;
    this.ship.y = this.initY;
    this.ship.frame = 0;
    this.exhaust.x = this.initX + this.ExhaustOffsetX;
    this.exhaust.y = this.initY + this.ExhaustOffsetY;
  }

  // Ship Miscellaneous Callback Definition
  explodeComplete() {
    // Kill The Ship
    if (this.life > 0) {
      this.ship.kill();

      // Reset
      this.reset();
      
      // Revive It.
      setTimeout(() => { 
        this.ship.revive();
        this.exhaust.revive();
        
        // Add Tween Animation
        this.shipTween.start();
        this.exhaustTween.start();

        this.canOperate = true;
      }, 500);

      logger(`Ship Killed. Remain Life: ${this.life}`);
    } else {
      this.ship.destroy();
      logger('Ship Destroyed.');
      this.beforeDestroy.dispatch();
    }
  }
}