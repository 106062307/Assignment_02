// Green Ship

class GreenShip extends Ship {
  constructor(game, initX, initY, shipAsset, exhaustAsset, shotAsset, shipGroup, bulletGroup) {
    // Init Ship
    super(game, initX, initY, shipAsset, exhaustAsset, shotAsset, shipGroup, bulletGroup);

    // Override Default Constant
    this.MoveVelocity = 400;

    this.ExplodeFrameRate = 48

    // Shot Relate
    this.ShotVelocity = 600;
    this.ShotOffsetX = 17;
  }
}