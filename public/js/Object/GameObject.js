// Based Class For All Sprite Object

class GameObject {
  constructor(game) {
    // Add Game Object
    this.game = game;
  }

  /**
   * @name addSprite Sprite Initialization.
   * @param { Object } options, Sprite Setting. Field x, y, key is requirement.
   */
  addSprite(options) {
    // Error Handling
    if (!options) {
      logger('Options Not Defined At Class GameObject, addSprite.', true);
    }

    // Add Sprite
    let { x, y, key, frame, group } = options;
    let newGameObject;

    if (typeof frame !== 'undefined') {
      if (typeof group  !== 'undefined') {
        newGameObject = this.game.add.sprite(x, y, key, frame, group);
      }
      else newGameObject = this.game.add.sprite(x, y, key, frame);
    } else {
      newGameObject = this.game.add.sprite(x, y, key);
    }

    this.game.physics.enable(newGameObject);

    return newGameObject;
  }
}