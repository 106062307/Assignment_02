// Fire Dragon

class BlackGreenSpider extends Enemy {

  // Custom Constant
  GameSpeedCoe = 80;

  constructor(game, initX, initY, asset, explosionAsset, shotAsset, particleAsset, enemyGroup, shotGroup, ship) {
    super(game, initX, initY, asset, explosionAsset, shotAsset, particleAsset, enemyGroup, shotGroup, ship);

    // Set Spider Speed
    this.enemy.body.velocity.y = Global.level.gameSpeed * this.GameSpeedCoe;
  }
}