// Enemy Class

class Enemy extends GameObject {

  // Animation Rate
  AliveAnimationRate = 12;

  // Enemy Default Property
  GameSpeedCoe = 50;
  AttackCD = 2000;                       // in ms

  // Shot Related
  ShotVelocity = 200;
  
  /**
   * @name constructor
   * @param { Phaser.Game } game 
   * @param { Number } initX 
   * @param { Number } initY 
   * @param { String } enemyAsset Asset Name
   * @param { String } explosionAsset Asset Name
   * @param { Phaser.Group } group Enemy Group
   */
  constructor(game, initX, initY, asset, explosionAsset, shotAsset, particleAsset, group, shotGroup , ship) {
    super(game);

    // property
    this.initX = initX;
    this.initY = initY;
    this.explosionAsset = explosionAsset;
    this.shotAsset = shotAsset;
    this.particleAsset = particleAsset;

    // Flag
    this.isDying = false;

    // Target Ship
    this.targetShip = ship;

    // Shot Group
    this.shotGroup = shotGroup;

    // Add Sprite
    this.enemy = this.addSprite({
      x: this.initX,
      y: this.initY,
      key: asset,
      frame: 0,
      group
    });

    // SetupEnemy Out Of Bound Kill
    this.enemy.checkWorldBounds = true;
    this.enemy.outOfBoundsKill = true;

    // Set up On kill Call Back
    this.enemy.events.onKilled.add(this.onKill, this);

    // Add To Sprite Object
    this.enemy.die = () => {
      this.die();
    }

    // Alive Animation
    this.enemy.animations.add('alive', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], this.AliveAnimationRate, true);

    // Play Default Animation
    this.enemy.animations.play('alive');

    // Fire Interval and Add It To Instance
    this.fireInterval = null;
    this.startFire();
    this.enemy.startFire = () => { this.startFire() };
    this.enemy.clearFire = () => { this.clearFire() };
  }
  // Getter
  get x() {
    return this.enemy.centerX;
  }

  get y() {
    return this.enemy.centerY;
  }

  // Public Function
  startFire() {
    // Fire To Player
    this.fireInterval = setInterval(() => { if (this.enemy.alive && !this.targetShip.isDying) this.fire(); }, this.AttackCD * Global.level.enemyAttackCDRatio);
  }

  clearFire() {
    clearInterval(this.fireInterval);
  }

  // Enemy Miscellaneous Callback Definition
  /**
   * @name onKill
   */
  onKill() {
    setTimeout(() => {
      this.enemy.destroy();
    }, 1000);
  }

  /**
   * @name fire Fire Weapon!!!
   */
  fire() {
    // Fire Single Bullet, And Not Record It.
    let shot = this.addSprite({
      x: this.x,
      y: this.y,
      key: this.shotAsset,
      frame: 0,
      group: this.shotGroup
    });

    this.game.physics.enable(shot);

    shot.checkWorldBounds = true;
    shot.outOfBoundsKill = true;

    shot.events.onKilled.add(() => { setTimeout(() => { shot.destroy(); }, 1000) });

    // Add Initial Velocity
    // Calculate dx, dy
    let dx = this.targetShip.ship.centerX - this.enemy.centerX;
    let dy = this.targetShip.ship.centerY - this.enemy.centerY;
    let length =  Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
    shot.body.velocity.x = Math.floor(dx / length * this.ShotVelocity * Global.level.enemyAttackSpeedRatio);
    shot.body.velocity.y = Math.floor(dy / length * this.ShotVelocity * Global.level.enemyAttackSpeedRatio);
  }

  /**
   * @name die
   */
  die() {
    // Add Explosion
    if (!this.isDying) {
      this.isDying = true;
      let explosion = this.game.add.sprite(this.enemy.centerX - 32, this.enemy.centerY - 32, this.explosionAsset, 0);
      let emitter = this.game.add.emitter(this.enemy.centerX, this.enemy.centerY, 20);
      this.enemy.kill();

      // Animation
      // Prepare Frame, Python I Miss You......
      let frame = [];
      for (let i = 0; i < 24; i++) frame.push(i);

      explosion.animations.add('explode', frame, 16);
      explosion.events.onAnimationComplete.add(() => {
        explosion.destroy();
      })

      // Emitter
      emitter.makeParticles(this.particleAsset);
      emitter.setYSpeed(-150, 150);
      emitter.setXSpeed(-150, 150);
      emitter.setScale(2, 0, 2, 0, 800);
      emitter.gravity = 0;
      setTimeout(() => { emitter.destroy(); }, 1500);
      
      emitter.start(true, 1000, null, 20);
      explosion.animations.play('explode');
    }
  }
}