// Main File For Initial All Phaser Game

// Global Setting
var Global = {
  worldSize: {
    uiWidth: 168,
    width: 500,
    height: 683
  },
  backgroundOffset: 200,
  level: {
    gameSpeed: 1,
    enemyRespawnCD: 1000,      // ms
    enemyRespawnAmount: 1,
    enemyAttackSpeedRatio: 1,
    enemyAttackCDRatio: 1
  },
  defaultLevelInfo: {
    gameSpeed: 1,
    enemyRespawnCD: 1000,      // ms
    enemyRespawnAmount: 1,
    enemyAttackSpeedRatio: 1,
    enemyAttackCDRatio: 1
  }
}


// Create New Game
function main() {
  // Init Firebase
  firebase.initializeApp({
    apiKey: "AIzaSyCnEGqdL-ab5UlRqsjC6h0XXpWHhGQjptM",
    authDomain: "raiden-e6270.firebaseapp.com",
    databaseURL: "https://raiden-e6270.firebaseio.com",
    projectId: "raiden-e6270",
    storageBucket: "raiden-e6270.appspot.com",
    messagingSenderId: "1081565447212",
    appId: "1:1081565447212:web:7f044a6a633b9f09"
  });

  // Initial Game
  let game = new Phaser.Game(Global.worldSize.width + Global.worldSize.uiWidth, Global.worldSize.height);

  // Add To Global
  game.db = firebase.firestore();

  game.stateChange = new Phaser.Signal();

  // Add Game Change Event Handler
  game.stateChange.add((to) => {
    // MainGame
    game.state.start(to);
  })

  // Set Up Game State
  game.state.add('MainGame', new MainGame(game));
  game.state.add('EndGame', new EndGame(game));
  game.state.add('StartGame', new StartMenu(game));

  // Start MainGame
  game.state.start('StartGame');
}

main();